from database import Database


class DbStations(Database):
    """Some database functions to work with stations."""
    def get_all(self, station):
        sql = "SELECT * FROM stations WHERE UPPER(station) = '{}'".format(station)
        all_stat_info = self._read(sql)
        return None if len(all_stat_info) == 0 else all_stat_info

    def get_station(self, line):
        sql = "SELECT id FROM stations WHERE line = {}".format(line)
        stat_info = self._read(sql)
        return stat_info

    def get_transfer(self, num, line):
        sql = "SELECT * FROM stations WHERE trans{0} = {1}".format(num, line)
        stat_info = self._read(sql)
        return stat_info

"""
    def get_line(self, station):
        sql = "SELECT line FROM stations WHERE station = '{}'".format(station)
        line_info = self._read(sql)
        return line_info
    
    def get_time_up(self, station):
        sql = "SELECT time_up FROM stations WHERE station = '{}'".format(station)
        time_info = self._read(sql)
        return time_info

    def get_time_down(self, station):
        sql = "SELECT time_down FROM stations WHERE station = '{}'".format(station)
        time_info = self._read(sql)
        return time_info
"""