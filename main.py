from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
import database.users
from database.stations import DbStations
import logging
import numpy as np


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


STATION1, STATION2 = range(2)


def start(bot, update):
    user = add_user(update)

    reply_keyboard = ReplyKeyboardMarkup([['🗓 Расписание']],
                                         resize_keyboard=True,
                                         one_time_keyboard=True)
    update.message.reply_text(
        'Добро пожаловать в бот Last Train!\n'
        'Я помогу успеть на последний поезд в Питерской подземке.')
    update.message.reply_text('Нажмите на кнопку 🗓 Расписание, что бы посмотреть расписание работы станций',
                              reply_markup=reply_keyboard)


def pre_search(bot, update):
    update.message.reply_text('Введите станцию отправления:',
                              reply_markup=ReplyKeyboardRemove())

    return STATION1


def search_second(bot, update, user_data):
    user = update.message.from_user
    user_data['station1'] = update.message.text

    dbstations = database.stations.DbStations()
    station_find = dbstations.get_all(station=user_data['station1'].upper())
    try:
        if station_find == None:
            update.message.reply_text(
                'Станция не найдена.\n'
                'Введите станцию отправления:')
            return
        else:
            update.message.reply_text('Введите станцию назначения:')
    except Exception as ex:
        logger.error(str(ex))

    logger.info("Station1 of %s: %s", user.first_name, update.message.text)

    return STATION2


def binary_search_station(station_list, item, low):
    high = len(station_list)

    while low <= high:
        mid = (low + high) // 2
        guess = station_list[mid]
        if guess == item:
            return mid
        elif guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None


def create_marshrut(bot, update, station1, station2):
    db_all_stations = np.array(DbStations().get_station(line=station1[0][2]))
    station_list = []
    for st in db_all_stations.flat:
        station_list.append(st)

    index_st1 = binary_search_station(station_list, station1[0][0], 0)
    index_st2 = binary_search_station(station_list, station2[0][0], index_st1)

    if index_st2 == None:
        update.message.reply_text("Последний поезд со станции {0} в сторону станции {1} отправится в {2}".format(
            station1[0][1], station2[0][1], str(station1[0][4])))
    else:
        update.message.reply_text("Последний поезд со станции {0} в сторону станции {1} отправится в {2}".format(
            station1[0][1], station2[0][1], str(station1[0][3])))


def find_station(bot, update, user_data):
    user = update.message.from_user
    user_data['station2'] = update.message.text

    reply_keyboard = ReplyKeyboardMarkup([['🗓 Расписание']],
                                         resize_keyboard=True,
                                         one_time_keyboard=True)

    dbstations = database.stations.DbStations()
    station1_find = dbstations.get_all(station=user_data['station1'].upper())
    station2_find = dbstations.get_all(station=user_data['station2'].upper())
    try:
        if station2_find == None:
            update.message.reply_text(
                'Станция не найдена.\n'
                'Введите станцию назначения:')
            return STATION2
        else:
            if station1_find[0][2] == station2_find[0][2]:

                update.message.reply_text("Ваш маршрут: {0} - {1}".format(
                    station1_find[0][1], station2_find[0][1]))

                create_marshrut(bot, update, station1_find, station2_find)
            elif station1_find[0][2] == 3 and station2_find[0][2] == 5:
                if (station1_find[0][0] in range(43, 49)) and (station2_find[0][0] in range(58, 62)):
                    trans_station0 = dbstations.get_all(station='Маяковская'.upper())
                    trans_station1 = dbstations.get_all(station='Площадь Восстания'.upper())
                else:
                    trans_station0 = dbstations.get_all(station='Гостиный двор'.upper())
                    trans_station1 = dbstations.get_all(station='Невский проспект'.upper())

                trans_station2 = DbStations().get_transfer(num=station2_find[0][2], line=trans_station1[0][2])
                trans_station3 = DbStations().get_transfer(num=trans_station1[0][2], line=station2_find[0][2])

                update.message.reply_text("Ваш маршрут: {0} - {1}|{2} - {3}|{4} - {5}".format(
                    station1_find[0][1], trans_station0[0][1], trans_station1[0][1],
                    trans_station2[0][1], trans_station3[0][1], station2_find[0][1]))

                create_marshrut(bot, update, station1_find, trans_station0)

                update.message.reply_text("❗️ Переход со станции {0} на станцию {1} закрывается в {2}".format(
                    trans_station0[0][1], trans_station1[0][1], '0-15'))
                update.message.reply_text("‼️ Переход со станции {0} на станцию {1} закрывается в {2}".format(
                    trans_station2[0][1], trans_station3[0][1], '0-15'))


            elif station1_find[0][2] == 5 and station2_find[0][2] == 3:
                if station1_find[0][0] in range(63, 69):
                    trans_station0 = dbstations.get_all(station='Садовая'.upper())
                    trans_station1 = dbstations.get_all(station='Сенная площадь'.upper())
                else:
                    trans_station0 = dbstations.get_all(station='Звенигородская'.upper())
                    trans_station1 = dbstations.get_all(station='Пушкинская'.upper())

                trans_station2 = DbStations().get_transfer(num=station2_find[0][2], line=trans_station1[0][2])
                trans_station3 = DbStations().get_transfer(num=trans_station1[0][2], line=station2_find[0][2])

                update.message.reply_text("Ваш маршрут: {0} - {1}|{2} - {3}|{4} - {5}".format(
                    station1_find[0][1], trans_station0[0][1], trans_station1[0][1],
                    trans_station2[0][1], trans_station3[0][1], station2_find[0][1]))

                create_marshrut(bot, update, station1_find, trans_station0)

                update.message.reply_text("❗️ Переход со станции {0} на станцию {1} закрывается в {2}".format(
                    trans_station0[0][1], trans_station1[0][1], '0-15'))
                update.message.reply_text("‼️ Переход со станции {0} на станцию {1} закрывается в {2}".format(
                    trans_station2[0][1], trans_station3[0][1], '0-15'))

            else:
                trans_station1 = DbStations().get_transfer(num=station2_find[0][2], line=station1_find[0][2])
                trans_station2 = DbStations().get_transfer(num=station1_find[0][2], line=station2_find[0][2])

                update.message.reply_text("Ваш маршрут: {0} - {1}|{2} - {3}".format(
                    station1_find[0][1], trans_station1[0][1], trans_station2[0][1], station2_find[0][1]))

                create_marshrut(bot, update, station1_find, trans_station1)

                update.message.reply_text("❗️ Переход со станции {0} на станцию {1} закрывается в {2}".format(
                    trans_station1[0][1], trans_station2[0][1], '0-15'))

    except Exception as ex:
        logger.error(str(ex))

    logger.info("Station2 of %s: %s", user.first_name, update.message.text)

    update.message.reply_text('Нажмите на кнопку 🗓 Расписание, что бы посмотреть расписание других станций',
                              reply_markup=reply_keyboard)

    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('До новых встреч!')

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def add_user(update):
    dbusers = database.users.DbUsers()
    user = dbusers.get_single(user_id=update.message.from_user.id)
    try:
        if user == None:
            new_user = []
            new_user.append(update.message.from_user.id)
            new_user.append(update.message.chat.username)
            new_user.append(update.message.chat.first_name)
            new_user.append(update.message.chat.last_name)
            dbusers.add_new(new_user)
        else:
            pass
    except Exception as ex:
        logger.error(str(ex))


def main():

    updater = Updater("760922135:AAETw5hLh1Z6WRXhwF674m8ohaTphxCheFU")

    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))

    conv_handler = ConversationHandler(
        entry_points=[RegexHandler('^🗓 Расписание$', pre_search)],

        states={
            STATION1: [MessageHandler(Filters.text,
                                      search_second,
                                      pass_user_data=True)],

            STATION2: [MessageHandler(Filters.text,
                                      find_station,
                                      pass_user_data=True)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    dp.add_error_handler(error)

    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()