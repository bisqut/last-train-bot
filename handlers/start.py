from telegram import ReplyKeyboardMarkup
import database.users
import logging
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

STATION1, STATION2 = range(2)


def start(bot, update):
    user = add_user(update)

    reply_keyboard = ReplyKeyboardMarkup([['🗓 Расписание']],
                                         resize_keyboard=True)
    update.message.reply_text(
        'Добро пожаловать в бот Last Train!\n'
        'Я помогу тебе успеть на последний поезд в питерской подземке.',
        reply_markup=reply_keyboard)


def pre_search(bot, update):
    update.message.reply_text(
        'Введите станцию отправления:')

    return STATION1


def search_second(bot, update):
    user = update.message.from_user
    logger.info("Station1 of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Введите станцию назначения:')

    return STATION2


def find_station(bot, update,):
    user = update.message.from_user
    logger.info("Station2 of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Последний поезд со станции ' + str(STATION1) +
                              ' в сторону ' + str(STATION2) + ' отправится в 0-10')

    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('До новых встреч!')

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def add_user(update):
    dbusers = database.users.DbUsers()
    user = dbusers.get_single(user_id=update.message.from_user.id)
    try:
        if user == None:
            new_user = []
            new_user.append(update.message.from_user.id)
            new_user.append(update.message.chat.username)
            new_user.append(update.message.chat.first_name)
            new_user.append(update.message.chat.last_name)
            dbusers.add_new(new_user)
        else:
            pass
    except Exception as ex:
        logger.error(str(ex))


conv_handler = ConversationHandler(
    entry_points=[RegexHandler('^🗓 Расписание$', pre_search, pass_user_data=True)],

    states={
        STATION1: [RegexHandler('^🗓 Расписание$', pre_search, pass_user_data=True)],

        STATION2: [MessageHandler(Filters.text, search_second, pass_user_data=True)]
    },

    fallbacks=[CommandHandler('cancel', cancel)]
)
