import logging
from telegram import ReplyKeyboardRemove
from telegram.ext import (ConversationHandler, MessageHandler, Filters,
                          CommandHandler, RegexHandler)
import database.forms

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

STATION1, STATION2 = range(2)


def pre_search(bot, update, user_data):
    update.message.reply_text(
        'Введите станцию отправления:')

    return STATION1


def search_second(bot, update, user_data):
    user_data['station1'] = update.message.text
    user = update.message.from_user
    logger.info("Station1 of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Введите станцию назначения:')

    return STATION2


def find_station(bot, update, user_data):
    user_data['station2'] = update.message.text
    user = update.message.from_user
    logger.info("Station2 of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Последний поезд со станции ' + str(STATION1) +
                              ' в сторону ' + str(STATION2) + ' отправится в 0-10')

    return ConversationHandler.END


    form_info = []
    form_info.append(user_data['station1'])
    form_info.append(user_data['station2'])
    form_info.append(user_data['username'])
    form_info.append(user_data['user_id'])
    dbform = database.forms.DbForms()
    dbform.create_form(form_info)
    bot.send_message(chat_id=523792555, text='You got new form from @' + str(user_data['username'])
                     + ' id:' + str(user_data['user_id']))
    user_data.clear()
    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


conv_handler = ConversationHandler(
    entry_points=[RegexHandler('^🗓 Расписание$', pre_search, pass_user_data=True)],

    states={
        STATION1: [MessageHandler(Filters.text, pre_search, pass_user_data=True)],

        STATION2: [MessageHandler(Filters.text, search_second, pass_user_data=True)]
    },

    fallbacks=[CommandHandler('cancel', cancel)]
)
